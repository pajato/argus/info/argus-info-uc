package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.tks.tv.core.Tv

internal fun getTvOrNull(repo: InfoRepo, key: InfoKey): Tv? {
    val item = repo.cache[key]
    return if (item is TimestampedTv) item.tv else null
}
