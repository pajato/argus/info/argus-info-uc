package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.getTvPosterPath
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class PosterPathTvUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a poster path, verify behavior`() = runBlocking {
        val expectedPath = "/eyTu5c8LniVciRZIOSHTvvkkgJa.jpg"
        val key = InfoKey(Tv.name, 1911)
        assertEquals(expectedPath, getTvPosterPath(TestInfoRepo, key))
        assertEquals(expectedPath, getTvPosterPath(TestInfoRepo, key, "Foo"))
    }

    @Test fun `When accessing a non-existent poster path, verify behavior`() = runBlocking {
        val expectedPath = ""
        val key = InfoKey(Tv.name, 1922)
        assertEquals(expectedPath, getTvPosterPath(TestInfoRepo, key, ""))
    }
}
