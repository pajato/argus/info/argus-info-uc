package com.pajato.argus.info.uc

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetVideoSeriesCountUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing an series count for a tv show, verify correct behavior`() {
        val expectedCount = 12
        val infoId = 1911
        val actualCount = TvUseCases.getSeriesCount(TestInfoRepo, infoId)
        assertEquals(expectedCount, actualCount)
    }
}
