package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.isLastEpisode
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class IsLastEpisodeUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val key = InfoKey(Tv.name, 1911)

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When a registered, ended tv episode is used, verify result`() {
        val canceledKey = InfoKey(Tv.name, 104359)
        val returningKey = InfoKey(Tv.name, 14929)
        assertTrue(isLastEpisode(TestInfoRepo, key, 12, 12))
        assertFalse(isLastEpisode(TestInfoRepo, key, 12, 11))
        assertFalse(isLastEpisode(TestInfoRepo, key, 11, 12))
        assertTrue(isLastEpisode(TestInfoRepo, canceledKey, 2, 8))
        assertFalse(isLastEpisode(TestInfoRepo, returningKey, 17, 10))
    }

    @Test fun `When a tv episode is not the last one, verify result`() {
        assertFalse(isLastEpisode(TestInfoRepo, key, 1, 1))
    }

    @Test fun `When an unregistered episode used, verify result`() {
        assertFalse(isLastEpisode(TestInfoRepo, InfoKey(Tv.name, -1), 1, 1))
    }
}
