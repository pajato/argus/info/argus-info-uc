package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.MovieUseCases.getMovieOverview
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class OverviewUnitTest : ReportingTestProfiler() {
    val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a movie overview, verify behavior`() = runBlocking {
        val expectedPath = """Nicholas Nickleby, a young boy in search of a better life, struggles to save his """ +
            """family and friends from the abusive exploitation of his coldheartedly grasping uncle."""
        val key = InfoKey(Movie.name, 29339)
        assertEquals(expectedPath, getMovieOverview(TestInfoRepo, key))
    }

    @Test fun `When accessing a non-existent poster path, verify behavior`() = runBlocking {
        val expectedPath = ""
        val key = InfoKey(Movie.name, 1922)
        assertEquals(expectedPath, getMovieOverview(TestInfoRepo, key))
    }

    @Test fun `When accessing a tv overview, verify behavior`() = runBlocking {
        val expectedPath = """Dr. Temperance Brennan and her colleagues at the Jeffersonian's Medico-Legal Lab """ +
            """assist Special Agent Seeley Booth with murder investigations when the remains are so badly """ +
            """decomposed, burned or destroyed that the standard identification methods are useless."""
        val key = InfoKey(Tv.name, 1911)
        assertEquals(expectedPath, TvUseCases.getTvOverview(TestInfoRepo, key))
    }

    @Test fun `When accessing a non-existent tv overview, verify behavior`() = runBlocking {
        val expectedPath = ""
        val key = InfoKey(Tv.name, 1922)
        assertEquals(expectedPath, TvUseCases.getTvOverview(TestInfoRepo, key))
    }
}
