package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import com.pajato.tks.movie.adapter.TmdbMovieRepo
import com.pajato.tks.season.adapter.TmdbSeasonRepo
import com.pajato.tks.tv.adapter.TmdbTvRepo
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.test.fail

class RegisterUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val tmdbRepos = listOf(TmdbMovieRepo, TmdbTvRepo, TmdbSeasonRepo, TmdbEpisodeRepo)
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        TestInfoRepo.registerHappened = false
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
        tmdbRepos.forEach { _ ->
            TmdbFetcher.inject(TmdbAgent::fetchResponse, TmdbAgent::handleError)
            TmdbFetcher.inject(API_KEY)
        }
    }

    @Test fun `When registering a tv show, verify a register operation takes place`() {
        val key = InfoKey(Tv.name, 0)
        TestInfoRepo.registerHappened = false
        runBlocking { TvUseCases.registerTv(TestInfoRepo, key) }
        assertTrue(TestInfoRepo.registerHappened)
    }

    @Test fun `When registering a movie item, verify a register operation takes place`() {
        val key = InfoKey(Movie.name, 0)
        TestInfoRepo.registerHappened = false
        runBlocking { MovieUseCases.registerMovie(TestInfoRepo, key) }
        assertTrue(TestInfoRepo.registerHappened)
    }
}
