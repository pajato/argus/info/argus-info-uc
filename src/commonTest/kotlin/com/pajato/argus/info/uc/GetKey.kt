package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoType
import com.pajato.dependency.uri.validator.InjectError

internal fun getKey(parts: List<String>, uri: String): InfoType = when (parts[1]) {
    "tv" -> InfoType.Tv
    "movie" -> InfoType.Movie
    "person" -> InfoType.Person
    else -> throw InjectError("Invalid URI (type): $uri")
}
