package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.tks.common.core.MovieKey
import com.pajato.tks.movie.adapter.TmdbMovieRepo
import com.pajato.tks.movie.core.Movie

public object MovieUseCases {
    private const val MOVIE = "Movie"

    public fun getMovieBackdropPath(repo: InfoRepo, infoId: Int): String =
        getMovieBackdropPath(repo, InfoKey(MOVIE, infoId))

    public fun getMovieBackdropPath(repo: InfoRepo, key: InfoKey, defaultPath: String = ""): String {
        val movie = getMovieOrNull(repo, key) ?: return defaultPath
        return movie.backdropPath
    }

    public fun getMoviePosterPath(repo: InfoRepo, infoId: Int): String =
        getMoviePosterPath(repo, InfoKey(MOVIE, infoId))

    public fun getMoviePosterPath(repo: InfoRepo, key: InfoKey, defaultPath: String = ""): String {
        val movie = getMovieOrNull(repo, key) ?: return defaultPath
        return movie.posterPath
    }

    public fun getMovieOverview(repo: InfoRepo, key: InfoKey): String {
        val movie: Movie = getMovieOrNull(repo, key) ?: return ""
        return movie.overview
    }

    public fun getMovieRating(repo: InfoRepo, key: InfoKey): String {
        val movie: Movie = getMovieOrNull(repo, key) ?: return ""
        return "${(movie.voteAverage * 10).toInt()}% (${movie.voteCount})"
    }

    public fun getMovieReleaseDate(repo: InfoRepo, key: InfoKey): String {
        val movie: Movie = getMovieOrNull(repo, key) ?: return ""
        return movie.releaseDate
    }

    public fun getMovieRuntime(repo: InfoRepo, key: InfoKey): Int {
        val movie: Movie = getMovieOrNull(repo, key) ?: return 0
        return movie.runtime
    }

    public fun getPartCount(repo: InfoRepo, infoId: Int): Int = getPartCount(repo, InfoKey(MOVIE, infoId))

    public fun getPartCount(repo: InfoRepo, key: InfoKey): Int {
        val parts = getMovieOrNull(repo, key)?.belongsToCollection?.parts
        val size = parts?.size
        return size ?: 1
    }

    public fun getMovieTitle(repo: InfoRepo, infoId: Int): String = getMovieTitle(repo, InfoKey(MOVIE, infoId))

    public fun getMovieTitle(repo: InfoRepo, key: InfoKey): String = getMovieOrNull(repo, key)?.title ?: ""

    public suspend fun registerMovie(repo: InfoRepo, key: InfoKey): Unit =
        repo.register(TimestampedMovie(0L, TmdbMovieRepo.getMovie(MovieKey(key.id))))

    private fun getMovieOrNull(repo: InfoRepo, key: InfoKey): Movie? {
        val item = repo.cache[key]
        return if (item is TimestampedMovie) item.movie else null
    }
}
