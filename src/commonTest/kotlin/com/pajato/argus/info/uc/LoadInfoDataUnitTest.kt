package com.pajato.argus.info.uc

import com.pajato.argus.info.core.I18nStrings.INFO_LOAD_ERROR_KEY
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class LoadInfoDataUnitTest : ReportingTestProfiler() {
    @Test fun `When loading the movie file, verify movie repo size`() {
        val loader = this::class.java.classLoader
        val infoPath = "files/info.txt"
        val uri = loader.getResource(infoPath)?.toURI() ?: fail(get(INFO_LOAD_ERROR_KEY, Arg("path", infoPath)))
        runBlocking {
            CommonUseCases.loadInfoData(TestInfoRepo, uri)
            assertEquals(6, TestInfoRepo.cache.values.size)
        }
    }
}
