package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.getEpisodeCount
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetEpisodeCountUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing an episode count for a series using a key, verify behavior`() {
        val key = InfoKey(Tv.name, 1911)
        assertEquals(-1, getEpisodeCount(TestInfoRepo, key, -100, -1))
        assertEquals(-2, getEpisodeCount(TestInfoRepo, key, 100, -2))
        assertEquals(22, getEpisodeCount(TestInfoRepo, key, 1, 0))
        assertEquals(22, getEpisodeCount(TestInfoRepo, key, 1))
        assertEquals(0, getEpisodeCount(TestInfoRepo, InfoKey(Tv.name, 0), 1))
    }

    @Test fun `When accessing an episode count for a series using an info id, verify behavior`() {
        val id = 1911
        assertEquals(0, getEpisodeCount(TestInfoRepo, id, -100))
        assertEquals(0, getEpisodeCount(TestInfoRepo, id, 100))
        assertEquals(23, getEpisodeCount(TestInfoRepo, id, 6))
        assertEquals(22, getEpisodeCount(TestInfoRepo, id, 1))
        assertEquals(47, getEpisodeCount(TestInfoRepo, id, 0))
    }
}
