package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepoError
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.getTvName
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class GetTvNameUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        com.pajato.argus.info.uc.TestInfoRepo.cache.clear()
        runBlocking { com.pajato.argus.info.uc.TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing cached and non-cached names, verify results`() {
        val key = InfoKey(Tv.name, 1911)
        runBlocking {
            assertEquals("Bones", getTvName(com.pajato.argus.info.uc.TestInfoRepo, key))
            assertFailsWith<InfoRepoError> { getTvName(com.pajato.argus.info.uc.TestInfoRepo, InfoKey(Tv.name, 0)) }
        }
    }
}
