package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.RefreshParams
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.tks.common.core.MovieKey
import com.pajato.tks.common.core.PersonKey
import com.pajato.tks.common.core.TvKey
import com.pajato.tks.movie.adapter.TmdbMovieRepo
import com.pajato.tks.person.adapter.TmdbPersonRepo
import com.pajato.tks.tv.adapter.TmdbTvRepo
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

internal suspend fun RefreshParams.performRefreshUpdate() {
    val now = System.currentTimeMillis()
    val items = repo.cache.values.toList()
    if (items.isEmpty()) handleNoItemsError() else coroutineScope { launch(IO) { performRefreshUpdate(items, now) } }
}

private fun RefreshParams.handleNoItemsError() {
    val errorMessage = """There are no items in the cache to refresh!"""
    onError(errorMessage)
}

private suspend fun RefreshParams.performRefreshUpdate(items: List<InfoWrapper>, now: Long) = items.forEach { item ->
    val errorMessage = """The media item "${getLabel(item)}" could not be refreshed!"""
    if (isValid(getFreshItem(item, now))) repo.register(getFreshItem(item, now)) else onError(errorMessage)
    delay(sendPeriod)
}

internal fun InfoWrapper.isStale(now: Timestamp): Boolean = when (this) {
    is TimestampedTv -> refreshTime < now
    is TimestampedMovie -> refreshTime < now
    is TimestampedPerson -> refreshTime < now
}

internal suspend fun getFreshItem(item: InfoWrapper, timestamp: Timestamp): InfoWrapper = when (item) {
    is TimestampedTv -> TimestampedTv(timestamp, TmdbTvRepo.getTv(TvKey(item.tv.id)))
    is TimestampedMovie -> TimestampedMovie(timestamp, TmdbMovieRepo.getMovie(MovieKey(item.movie.id)))
    is TimestampedPerson -> TimestampedPerson(timestamp, TmdbPersonRepo.getPerson((PersonKey(item.person.id))))
}

internal fun isValid(item: InfoWrapper): Boolean = when (item) {
    is TimestampedTv -> item.tv.id > 0
    is TimestampedMovie -> item.movie.id > 0
    is TimestampedPerson -> item.person.id > 0
}

internal fun getLabel(item: InfoWrapper) = when (item) {
    is TimestampedTv -> item.tv.name
    is TimestampedMovie -> item.movie.title
    is TimestampedPerson -> item.person.name
}
