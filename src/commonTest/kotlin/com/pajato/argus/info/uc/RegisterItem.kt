package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoType

internal suspend fun registerItem(type: InfoType, json: String) = when (type) {
    InfoType.Movie -> TestInfoRepo.register(json)
    InfoType.Person -> TestInfoRepo.register(json)
    InfoType.Tv -> TestInfoRepo.register(json)
}
