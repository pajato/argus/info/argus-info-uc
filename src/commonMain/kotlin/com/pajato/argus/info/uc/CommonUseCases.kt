package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.RefreshParams
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.argus.network.core.NetworkRepo
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.net.URI

public object CommonUseCases {

    /**
     * Fetches the list of genre IDs for a given video from the repository.
     *
     * @param repo The repository containing video information.
     * @param infoKey The key used to retrieve the specific video's information from the repository.
     *
     * @return A list of genre IDs associated with the specified video. Returns an empty list if no data is found.
     */
    public fun getVideoGenres(repo: InfoRepo, infoKey: InfoKey): List<Int> {
        val wrapper: InfoWrapper = repo.cache[infoKey] ?: return emptyList()
        val genres = getGenres(wrapper)
        return genres
    }

    private fun getGenres(wrapper: InfoWrapper): List<Int> = when (wrapper) {
        is TimestampedMovie -> wrapper.movie.genres.map { it.id }
        is TimestampedTv -> wrapper.tv.genres.map { it.id }
        is TimestampedPerson -> emptyList()
    }

    /**
     * Loads information data into the given repository by injecting a dependency.
     *
     * @param repo The information repository where the data will be loaded.
     * @param uri The URI of the resource containing the information to be loaded.
     * @return Unit
     */
    public fun loadInfoData(repo: InfoRepo, uri: URI): Unit = runBlocking { repo.injectDependency(uri) }

    /**
     * Performs a series of refresh updates based on the provided parameters.
     *
     * @param params The parameters defining the refresh updates, including the number of updates,
     * the interval between updates, and the action to perform during each refresh.
     * @return Nothing (Unit). This method runs continuously for the defined number of updates and
     * delays according to the specified refresh interval.
     */
    public suspend fun performRefreshUpdates(params: RefreshParams): Unit = repeat(params.refreshCount) {
        params.performRefreshUpdate()
        delay(params.refreshPeriod)
    }

    /**
     * Retrieves the name of the network associated with the given network ID from the specified repository.
     *
     * @param repo The repository containing network information.
     * @param networkId The ID of the network whose name is to be retrieved.
     * @return The name of the network if found, otherwise "Unknown".
     */
    public fun getNetworkName(repo: NetworkRepo, networkId: Int): String {
        val selectableNetwork = repo.cache[networkId] ?: return "Unknown"
        return selectableNetwork.network.label
    }
}
