package com.pajato.argus.info.uc

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetVideoNameUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a name for a movie, verify correct behavior`() {
        val expectedName = "Nicholas Nickleby"
        val infoId = 29339
        val actualName = MovieUseCases.getMovieTitle(TestInfoRepo, infoId)
        assertEquals(expectedName, actualName)
    }

    @Test fun `When accessing a name for a tv show, verify correct behavior`() {
        val expectedName = "Bones"
        val infoId = 1911
        val actualName = TvUseCases.getTvName(TestInfoRepo, infoId)
        assertEquals(expectedName, actualName)
    }
}
