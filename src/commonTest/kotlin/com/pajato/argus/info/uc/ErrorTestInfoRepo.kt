package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoWrapper
import java.net.URI

object ErrorTestInfoRepo : InfoRepo {
    override val cache: MutableMap<InfoKey, InfoWrapper> = mutableMapOf()
    override suspend fun injectDependency(uri: URI) { TODO("Not yet implemented") }
    override suspend fun register(json: String) { TODO("Not yet implemented") }
    override suspend fun register(item: InfoWrapper) { TODO("Not yet implemented") }
    fun fetchResponse(dummy: String): String = ""
    internal fun handleError(message: String, exc: Exception?) {}
}
