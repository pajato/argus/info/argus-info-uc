package com.pajato.argus.info.uc

import com.pajato.argus.network.core.NetworkRepo
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.persister.readAndPruneData
import java.net.URI

object TestNetworkRepo : NetworkRepo {
    override val cache: MutableMap<Int, SelectableNetwork> = mutableMapOf()
    private var networkUri: URI? = null

    override suspend fun injectDependency(uri: URI) {
        fun handler(exc: Throwable) { throw exc }
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, SelectableNetwork.serializer(), ::getKeyFromItem)
        networkUri = uri
    }

    override fun register(item: SelectableNetwork) { TODO("Not yet implemented") }

    override fun toggleHidden(item: SelectableNetwork) { TODO("Not yet implemented") }

    override fun register(json: String) { TODO("Not yet implemented") }

    override fun toggleSelected(item: SelectableNetwork) { TODO("Not yet implemented") }

    private fun getKeyFromItem(item: SelectableNetwork): Int = item.network.id
}
