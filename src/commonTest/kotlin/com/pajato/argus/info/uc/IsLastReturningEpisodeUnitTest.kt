package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.isLastReturningEpisode
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class IsLastReturningEpisodeUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When a registered, returning tv episode is used, verify result`() {
        val returningKey = InfoKey(Tv.name, 14929)
        val endedKey = InfoKey(Tv.name, 1911)
        assertTrue(isLastReturningEpisode(TestInfoRepo, returningKey, 17, 10))
        assertFalse(isLastReturningEpisode(TestInfoRepo, returningKey, 17, 9))
        assertFalse(isLastReturningEpisode(TestInfoRepo, returningKey, 16, 10))
        assertFalse(isLastReturningEpisode(TestInfoRepo, endedKey, 12, 12))
    }

    @Test fun `When an unregistered episode used, verify result`() {
        assertFalse(isLastReturningEpisode(TestInfoRepo, InfoKey(Tv.name, -1), 1, 1))
    }
}
