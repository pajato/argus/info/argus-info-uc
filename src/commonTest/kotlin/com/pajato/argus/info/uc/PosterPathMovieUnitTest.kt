package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.uc.MovieUseCases.getMoviePosterPath
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class PosterPathMovieUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a poster path, verify behavior`() = runBlocking {
        val expectedPath = "/41AJ3hIEIuJ8ltlzZ0ZfHoSvYbe.jpg"
        val key = InfoKey(Movie.name, 29339)
        assertEquals(expectedPath, getMoviePosterPath(TestInfoRepo, key))
        assertEquals(expectedPath, getMoviePosterPath(TestInfoRepo, key, "Foo"))
    }

    @Test fun `When accessing a non-existent poster path, verify behavior`() = runBlocking {
        val expectedPath = ""
        val key = InfoKey(Movie.name, 1922)
        assertEquals(expectedPath, getMoviePosterPath(TestInfoRepo, key, ""))
    }
}
