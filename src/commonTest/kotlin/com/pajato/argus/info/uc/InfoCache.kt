package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoWrapper

typealias InfoCache = MutableMap<InfoKey, InfoWrapper>
