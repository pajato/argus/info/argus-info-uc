package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.persister.jsonFormat
import com.pajato.tks.common.core.MovieKey
import com.pajato.tks.common.core.PersonKey
import com.pajato.tks.common.core.TvKey
import com.pajato.tks.movie.adapter.TmdbMovieRepo
import com.pajato.tks.person.adapter.TmdbPersonRepo
import com.pajato.tks.tv.adapter.TmdbTvRepo
import java.io.File
import java.net.URI

internal suspend fun persist(key: InfoKey, item: InfoWrapper, uri: URI) {
    val file = File(uri)
    val newItem = replace(key, item)
    val json = jsonFormat.encodeToString(InfoWrapper.serializer(), newItem)
    file.appendText("$json\n")
}

private suspend fun replace(key: InfoKey, item: InfoWrapper): InfoWrapper = when (item) {
    is TimestampedTv -> item.copy(tv = TmdbTvRepo.getTv(TvKey(key.id)))
    is TimestampedMovie -> item.copy(movie = TmdbMovieRepo.getMovie(MovieKey(key.id)))
    is TimestampedPerson -> item.copy(person = TmdbPersonRepo.getPerson(PersonKey(key.id)))
}
