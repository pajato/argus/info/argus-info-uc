package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.isLastEpisodeInSeries
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class IsLastEpisodeInSeriesUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When a registered tv episode is used, verify result`() {
        val key = InfoKey(Tv.name, 1911)
        assertTrue(isLastEpisodeInSeries(TestInfoRepo, key, 12, 12))
        assertFalse(isLastEpisodeInSeries(TestInfoRepo, key, 12, 11))
        assertFalse(isLastEpisodeInSeries(TestInfoRepo, key, 13, 12))
    }

    @Test fun `When an unregistered episode used, verify result`() {
        assertFalse(isLastEpisodeInSeries(TestInfoRepo, InfoKey(Tv.name, -1), 1, 1))
    }
}
