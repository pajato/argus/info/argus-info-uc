package com.pajato.argus.info.uc

import com.pajato.argus.info.core.I18nStrings.INFO_URI_ERROR_KEY
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import com.pajato.tks.movie.core.Movie
import com.pajato.tks.person.core.Person
import com.pajato.tks.season.adapter.TmdbSeasonRepo
import com.pajato.tks.tv.adapter.TmdbTvRepo
import com.pajato.tks.tv.core.Tv
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class RefreshHelperUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val name = "refresh-files/info.txt"
    private val uri = loader.getResource(name)?.toURI() ?: fail(get(INFO_URI_ERROR_KEY, Arg("path", name)))

    @BeforeTest fun setUp() = runBlocking {
        val tmdbRepos = listOf(TmdbTvRepo, TmdbSeasonRepo, TmdbEpisodeRepo)
        RefreshTestInfoRepo.classLoader = loader
        CommonUseCases.loadInfoData(RefreshTestInfoRepo, uri)
        tmdbRepos.forEach { _ ->
            TmdbFetcher.inject(RefreshTestInfoRepo::fetchResponse, RefreshTestInfoRepo::handleError)
            TmdbFetcher.inject(API_KEY)
        }
    }

    @Test fun `When accessing a fresh movie item, verify behavior`() {
        var movie: InfoWrapper
        val now = System.currentTimeMillis()
        val item = TimestampedMovie(0L, Movie(id = 29339))
        runBlocking { movie = getFreshItem(item, now) }
        assertEquals(29339, (movie as TimestampedMovie).movie.id)
        assertTrue(item.isStale(now))
        assertTrue(isValid(item))
    }

    @Test fun `When accessing a fresh person item, verify behavior`() {
        var person: InfoWrapper
        val now = System.currentTimeMillis()
        val item = TimestampedPerson(0L, Person(id = 190405))
        runBlocking { person = getFreshItem(item, System.currentTimeMillis()) }
        assertEquals(190405, (person as TimestampedPerson).person.id)
        assertTrue(item.isStale(now))
        assertTrue(isValid(item))
        assertEquals("", getLabel(item))
    }

    @Test fun `When accessing a stale movie item, verify behavior`() {
        val item = TimestampedMovie(0L, Movie(id = 29339))
        assertFalse(item.isStale(0L))
    }

    @Test fun `When accessing a fresh tv item, verify behavior`() {
        val item = TimestampedTv(10L, Tv(id = 1911))
        assertFalse(item.isStale(0L))
    }

    @Test fun `When accessing a stale tv item, verify behavior`() {
        val item = TimestampedTv(0L, Tv(id = 1911))
        val invalidItem = TimestampedTv(0L, Tv(id = 0))
        assertTrue(item.isStale(10L))
        assertFalse(isValid(invalidItem))
    }
}
