package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.TvUseCases.getSeriesCount
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetTvSeriesCountUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a series count, verify behavior`() {
        val key = InfoKey(Tv.name, 1911)
        runBlocking {
            assertEquals(12, getSeriesCount(TestInfoRepo, key))
            assertEquals(0, getSeriesCount(TestInfoRepo, InfoKey(Tv.name, 0)))
        }
    }
}
