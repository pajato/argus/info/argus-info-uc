package com.pajato.argus.info.uc

import com.pajato.argus.info.core.I18nStrings.INFO_URI_ERROR_KEY
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.RefreshParams
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.ErrorHandler
import com.pajato.tks.common.core.UrlFetcher
import com.pajato.tks.episode.adapter.TmdbEpisodeRepo
import com.pajato.tks.movie.core.Movie
import com.pajato.tks.season.adapter.TmdbSeasonRepo
import com.pajato.tks.tv.adapter.TmdbTvRepo
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue
import kotlin.test.fail

const val API_KEY = "dfd0ee93d011ad9182a9d9930215c02"
private const val REFRESH_PERIOD = 10L // ten milliseconds
private const val SEND_PERIOD = 1L // one millisecond

class PerformRefreshUpdateUnitTest {
    private val loader = this::class.java.classLoader
    private val uri = getUri("refresh-files/info.txt")

    @Test fun `When no items exist in the given repo, verify behavior`() {
        var (message, expected) = "" to "There are no items in the cache to refresh!"
        ErrorTestInfoRepo.cache.clear()
        runErrorTest(message, expected)
    }

    @Test fun `When accessing an errant movie item, verify behavior`() {
        var (message, expected) = "" to """The media item "" could not be refreshed!"""
        val item = TimestampedMovie(0L, Movie(id = 0))
        setUpForError()
        ErrorTestInfoRepo.cache[InfoKey(InfoType.Movie.name, 0)] = item
        runErrorTest(message, expected)
    }

    private fun runErrorTest(message: String, expected: String) {
        var message1 = message
        runBlocking {
            val params = RefreshParams(ErrorTestInfoRepo, 1, REFRESH_PERIOD, SEND_PERIOD) { message1 = it }
            CommonUseCases.performRefreshUpdates(params)
            assertEquals(expected, message1)
        }
    }

    @Test fun `When requesting no updates, verify immediate termination`() = runBlocking {
        val job = launch { failAfter(50L, "It should have terminated immediately!") }
        val params = RefreshParams(RefreshTestInfoRepo, 0, REFRESH_PERIOD, SEND_PERIOD) { }
        CommonUseCases.performRefreshUpdates(params)
        job.cancel()
    }

    private suspend fun failAfter(lengthInMs: Long, message: String) {
        delay(lengthInMs)
        fail("performRefreshUpdate() is still running after $lengthInMs ms. $message")
    }

    @Test fun `When requesting 1 update, verify delayed termination`() = runBlocking {
        val (startTime, repeatCount) = System.currentTimeMillis() to 1
        val params = RefreshParams(RefreshTestInfoRepo, repeatCount, REFRESH_PERIOD, SEND_PERIOD) { }
        CommonUseCases.performRefreshUpdates(params)
        assertDuration(System.currentTimeMillis() - startTime)
    }

    private fun assertDuration(duration: Long) {
        val message = "Duration was shorter than 10ms: ${duration}ms"
        assertTrue(duration >= REFRESH_PERIOD, message)
    }

    @Test fun `When updating a changed tv item, verify behavior`() {
        val key = InfoKey(InfoType.Tv.name, 1911)
        val params = RefreshParams(RefreshTestInfoRepo, 1, REFRESH_PERIOD, SEND_PERIOD) { }
        runBlocking {
            setUpForTest()
            runTest(key, params)
        }
    }

    private fun setUp(onFetch: UrlFetcher, onError: ErrorHandler) =
        listOf(TmdbTvRepo, TmdbSeasonRepo, TmdbEpisodeRepo).forEach {
            TmdbFetcher.inject(onFetch, onError)
            TmdbFetcher.inject(API_KEY)
        }

    private fun setUpForTest() = runBlocking {
        RefreshTestInfoRepo.classLoader = loader
        CommonUseCases.loadInfoData(RefreshTestInfoRepo, uri)
        setUp(RefreshTestInfoRepo::fetchResponse, RefreshTestInfoRepo::handleError)
    }

    private fun setUpForError() = runBlocking {
        setUp(ErrorTestInfoRepo::fetchResponse, ErrorTestInfoRepo::handleError)
    }

    private fun getUri(path: String): URI = loader.getResource(path)?.toURI() ?: fail(getUriErrorMessage(path))

    private fun getUriErrorMessage(path: String) = get(INFO_URI_ERROR_KEY, Arg("path", path))

    private suspend fun runTest(key: InfoKey, params: RefreshParams) {
        val itemBeforeRefresh = getFromInjectedRepo("refresh-stale-files/info.txt", key)
        CommonUseCases.performRefreshUpdates(params)
        assertNotEquals((itemBeforeRefresh as TimestampedTv).tv, (RefreshTestInfoRepo.cache[key] as TimestampedTv).tv)
        RefreshTestInfoRepo.register(itemBeforeRefresh)
    }

    private suspend fun getFromInjectedRepo(path: String, key: InfoKey): InfoWrapper {
        RefreshTestInfoRepo.injectDependency(getUri(path))
        return RefreshTestInfoRepo.cache[key] ?: fail("Cache failure!")
    }
}
