package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.Genre
import com.pajato.tks.common.core.TmdbId
import com.pajato.tks.movie.core.Movie
import com.pajato.tks.person.core.Person
import com.pajato.tks.tv.core.Tv
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class GetVideoGenresUnitTest : ReportingTestProfiler() {

    @Test fun `When accessing genres from a non-existent cache key, verify behavior`() {
        val id: TmdbId = 23
        val key = InfoKey(InfoType.Tv.name, id)
        val genreList = CommonUseCases.getVideoGenres(TestInfoRepo, key)
        assertTrue(genreList.isEmpty())
    }

    @Test fun `When accessing genres for a person item, verify behavior`() {
        val id: TmdbId = 48
        val key = InfoKey(InfoType.Person.name, id)
        TestInfoRepo.cache[key] = TimestampedPerson(0L, Person())
        runTest(key, 0)
    }

    @Test fun `When accessing genres for a tv item with a single genre, verify behavior`() {
        val id: TmdbId = 52
        val key = InfoKey(InfoType.Tv.name, id)
        TestInfoRepo.cache[key] = TimestampedTv(0L, Tv(genres = listOf(Genre(id, "test"))))
        runTest(key, 1)
    }

    @Test fun `When accessing genres for a movie item with two genre items, verify behavior`() {
        val id: TmdbId = 74
        val key = InfoKey(InfoType.Tv.name, id)
        val list = listOf(Genre(1, "test1"), Genre(2, "test2"))
        TestInfoRepo.cache[key] = TimestampedMovie(0L, Movie(genres = list))
        runTest(key, 2)
    }

    private fun runTest(key: InfoKey, expectedSize: Int) {
        val genreList = CommonUseCases.getVideoGenres(TestInfoRepo, key)
        assertEquals(expectedSize, genreList.size)
    }
}
