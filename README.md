# argus-info-uc

## Description

This project defines the Argus info feature
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
"Application Business Rules" layer (aka "Use Cases"). It provides the use cases for the TMDb info facility.

This project defines Argus video info use cases for the Use Case Clean Architecture layer.
It exists to specify the operations which are used by the video info use case layer to provide internet
acquired data. Tests exist to ensure that the interface designs are sensible. These tests
also show how the interfaces will likely get used.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the info feature.

## Test Cases

### Overview

The table below identifies the core layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions). The package is `com.pajato.argus.info.uc`

| Filename Prefix     | Test Case Name                                                                        |
|---------------------|---------------------------------------------------------------------------------------|
| GetEpisodeCount     | When accessing an episode count for a series using a key, verify behavior             |
|                     | When accessing an episode count for a series using an info id, verify behavior        |
| GetVideoName        | When accessing a name for a movie, verify correct behavior                            |
|                     | When accessing a name for a tv show, verify correct behavior                          |
| GetVideoNetworkName | When accessing a name for a movie with an unknown network id, verify correct behavior |
|                     | When accessing a name for a movie with a valid network id, verify correct behavior    |
| ...                 | ...                                                                                   |
