package com.pajato.argus.info.uc

import com.pajato.argus.info.core.I18nStrings.INFO_NULL_VALUE_KEY
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoRepoError
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.tks.common.core.TvKey
import com.pajato.tks.season.core.Season
import com.pajato.tks.tv.adapter.TmdbTvRepo
import com.pajato.tks.tv.core.Tv

public object TvUseCases {
    private const val TV = "Tv"

    public fun getTvName(repo: InfoRepo, infoId: Int): String = getTvName(repo, InfoKey(TV, infoId))

    public fun getTvName(repo: InfoRepo, key: InfoKey): String {
        val typeArg = Arg("type", "tv")
        val keyArg = Arg("key", "$key")
        val tv = getTvOrNull(repo, key) ?: throw InfoRepoError(get(INFO_NULL_VALUE_KEY, typeArg, keyArg))
        return tv.name
    }

    public fun getTvPosterPath(repo: InfoRepo, infoId: Int): String = getTvPosterPath(repo, InfoKey(TV, infoId))

    public fun getTvPosterPath(repo: InfoRepo, key: InfoKey, defaultPath: String = ""): String {
        val tv = getTvOrNull(repo, key) ?: return defaultPath
        return tv.posterPath
    }

    public fun getTvBackdropPath(repo: InfoRepo, infoId: Int): String = getTvBackdropPath(repo, InfoKey(TV, infoId))

    public fun getTvBackdropPath(repo: InfoRepo, key: InfoKey, defaultPath: String = ""): String {
        val tv = getTvOrNull(repo, key) ?: return defaultPath
        return tv.backdropPath
    }

    public fun getTvOverview(repo: InfoRepo, key: InfoKey, defaultPath: String = ""): String {
        val tv: Tv = getTvOrNull(repo, key) ?: return defaultPath
        return tv.overview
    }

    public fun getSeriesCount(repo: InfoRepo, infoId: Int): Int =
        getSeriesCount(repo, InfoKey(TV, infoId), 0)

    public fun getSeriesCount(repo: InfoRepo, key: InfoKey, defaultCount: Int = 0): Int {
        val tv = getTvOrNull(repo, key) ?: return defaultCount
        return tv.numberOfSeasons
    }

    public fun getEpisodeCount(repo: InfoRepo, infoId: Int, series: Int): Int =
        getEpisodeCount(repo, InfoKey(TV, infoId), series)

    public fun getEpisodeCount(repo: InfoRepo, key: InfoKey, series: Int, defaultCount: Int = 0): Int {
        val tv = getTvOrNull(repo, key) ?: return defaultCount
        val season = tv.seasons.find { it.seasonNumber == series } ?: return defaultCount
        return season.episodeCount
    }

    public fun isLastEpisode(repo: InfoRepo, key: InfoKey, series: Int, episode: Int): Boolean {
        val show = getTvOrNull(repo, key) ?: return false
        val lastEpisode = show.lastEpisodeToAir.episodeNumber
        val lastSeason = show.lastEpisodeToAir.seasonNumber
        return lastSeason == series && lastEpisode == episode && isFinished(repo, key)
    }

    public fun isFinished(repo: InfoRepo, key: InfoKey): Boolean {
        val tv: Tv = getTvOrNull(repo, key) ?: return false
        return tv.status == "Ended" || tv.status == "Canceled"
    }

    public fun isLastReturningEpisode(repo: InfoRepo, key: InfoKey, series: Int, episode: Int): Boolean {
        val show: Tv = getTvOrNull(repo, key) ?: return false
        val lastEpisode = show.lastEpisodeToAir.episodeNumber
        val lastSeason = show.lastEpisodeToAir.seasonNumber
        return lastSeason == series && lastEpisode == episode && show.status == "Returning Series"
    }

    public fun isLastEpisodeInSeries(repo: InfoRepo, key: InfoKey, series: Int, episode: Int): Boolean {
        val show: Tv = getTvOrNull(repo, key) ?: return false
        val season: Season = show.seasons.find { it.seasonNumber == series } ?: return false
        return episode == season.episodeCount
    }

    public suspend fun registerTv(repo: InfoRepo, key: InfoKey) {
        repo.register(TimestampedTv(System.currentTimeMillis(), TmdbTvRepo.getTv(TvKey(key.id))))
    }
}
