package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.uc.MovieUseCases.getMovieReleaseDate
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class MovieReleaseDateUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing the release date, verify behavior`() = runBlocking {
        val expectedRuntime = "2002-12-27"
        val key = InfoKey(Movie.name, 29339)
        assertEquals(expectedRuntime, getMovieReleaseDate(TestInfoRepo, key))
    }

    @Test fun `When accessing the release date for an unregistered movie id, verify behavior`() = runBlocking {
        val expectedRuntime = ""
        val key = InfoKey(Movie.name, 1922)
        assertEquals(expectedRuntime, getMovieReleaseDate(TestInfoRepo, key))
    }
}
