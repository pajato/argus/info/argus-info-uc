package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertFalse

class IsFinishedUnitTest : ReportingTestProfiler() {

    @Test fun `When an unregistered tv show is accessed, verify the behavior`() {
        assertFalse(TvUseCases.isFinished(TestInfoRepo, InfoKey(Tv.name, -1)))
    }
}
