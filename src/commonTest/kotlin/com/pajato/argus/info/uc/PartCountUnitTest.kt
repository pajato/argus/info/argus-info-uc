package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.uc.MovieUseCases.getPartCount
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class PartCountUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val infoId = 29339
    private val invalidId = 0

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a part count for a movie with an invalid id, verify the exception`() {
        runBlocking { assertEquals(1, getPartCount(TestInfoRepo, invalidId)) }
    }

    @Test fun `When accessing a part count for a movie without parts, verify the size`() {
        val expectedCount = 1
        val actualCount: Int = getPartCount(TestInfoRepo, infoId)
        assertEquals(expectedCount, actualCount)
    }

    @Test fun `When accessing a part count for a movie with parts, verify the size`() {
        val expectedCount = 0
        val harryPotterId = 671
        val actualCount: Int = getPartCount(TestInfoRepo, harryPotterId)
        assertEquals(expectedCount, actualCount)
    }

    @Test fun `When accessing a part count for a movie using an info id, verify correct behavior`() {
        val expectedCount = 1
        val actualCount = getPartCount(TestInfoRepo, infoId)
        assertEquals(expectedCount, actualCount)
    }

    @Test fun `When accessing a part count for a movie using a key, verify correct behavior`() {
        val expectedCount = 1
        val key = InfoKey(Movie.name, infoId)
        val actualCount: Int = getPartCount(TestInfoRepo, key)
        assertEquals(expectedCount, actualCount)
    }
}
