package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.MovieUseCases.getMovieBackdropPath
import com.pajato.argus.info.uc.TvUseCases.getTvBackdropPath
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetBackdropPathUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val emptyPath = ""
    private val defaultPath = "Foo"

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a tv backdrop path, verify behavior`() {
        val expectedPath = "/e9n87p3Ax67spq3eUgLB6rjIEow.jpg"
        runBlocking {
            assertEquals(expectedPath, getTvBackdropPath(TestInfoRepo, InfoKey(Tv.name, 1911)))
            assertEquals(emptyPath, getTvBackdropPath(TestInfoRepo, InfoKey(Tv.name, 0)))
            assertEquals(defaultPath, getTvBackdropPath(TestInfoRepo, InfoKey(Tv.name, 0), defaultPath))
        }
    }

    @Test fun `When accessing a movie backdrop path, verify behavior`() {
        val expectedPath = "/bvhAbUMsDfFTseAAoPE49JJ0ebA.jpg"
        runBlocking {
            assertEquals(expectedPath, getMovieBackdropPath(TestInfoRepo, InfoKey(Movie.name, 29339)))
            assertEquals(emptyPath, getMovieBackdropPath(TestInfoRepo, InfoKey(Movie.name, 0)))
            assertEquals(defaultPath, getMovieBackdropPath(TestInfoRepo, InfoKey(Movie.name, 0), defaultPath))
        }
    }

    @Test fun `When accessing a backdrop path for a movie, verify correct behavior`() {
        val expectedPath = "/bvhAbUMsDfFTseAAoPE49JJ0ebA.jpg"
        val infoId = 29339
        val actualPath = MovieUseCases.getMovieBackdropPath(TestInfoRepo, infoId)
        assertEquals(expectedPath, actualPath)
    }

    @Test fun `When accessing a backdrop path for a tv show, verify correct behavior`() {
        val expectedPath = "/e9n87p3Ax67spq3eUgLB6rjIEow.jpg"
        val infoId = 1911
        val actualPath = TvUseCases.getTvBackdropPath(TestInfoRepo, infoId)
        assertEquals(expectedPath, actualPath)
    }
}
