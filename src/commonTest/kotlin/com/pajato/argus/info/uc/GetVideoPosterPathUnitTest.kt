package com.pajato.argus.info.uc

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetVideoPosterPathUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/info.txt")?.toURI() ?: fail("Could not load resources!")
        TestInfoRepo.cache.clear()
        runBlocking { TestInfoRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a paster path for a movie, verify correct behavior`() {
        val expectedPath = "/41AJ3hIEIuJ8ltlzZ0ZfHoSvYbe.jpg"
        val infoId = 29339
        val actualPath = MovieUseCases.getMoviePosterPath(TestInfoRepo, infoId)
        assertEquals(expectedPath, actualPath)
    }

    @Test fun `When accessing a poster path for a tv show, verify correct behavior`() {
        val expectedPath = "/eyTu5c8LniVciRZIOSHTvvkkgJa.jpg"
        val infoId = 1911
        val actualPath = TvUseCases.getTvPosterPath(TestInfoRepo, infoId)
        assertEquals(expectedPath, actualPath)
    }
}
