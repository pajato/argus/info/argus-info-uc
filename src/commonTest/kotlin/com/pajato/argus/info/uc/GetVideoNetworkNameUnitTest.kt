package com.pajato.argus.info.uc

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetVideoNetworkNameUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val infoUri: URI = loader.getResource("files/networks.txt")?.toURI() ?: fail("Could not load resources!")
        TestNetworkRepo.cache.clear()
        runBlocking { TestNetworkRepo.injectDependency(infoUri) }
    }

    @Test fun `When accessing a name for a movie with an unknown network id, verify correct behavior`() {
        val expectedName = "Unknown"
        val networkId = -1
        val actualName = CommonUseCases.getNetworkName(TestNetworkRepo, networkId)
        assertEquals(expectedName, actualName)
    }

    @Test fun `When accessing a name for a movie with a valid network id, verify correct behavior`() {
        val expectedName = "Apple TV+"
        val networkId = 2552
        val actualName = CommonUseCases.getNetworkName(TestNetworkRepo, networkId)
        assertEquals(expectedName, actualName)
    }
}
