package com.pajato.argus.info.uc

import com.pajato.argus.info.core.InfoType

public typealias InfoId = InfoType
public typealias Timestamp = Long
