package com.pajato.argus.info.uc

import com.pajato.argus.info.core.I18nStrings.INFO_URI_ERROR_KEY
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoRepoError
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.persister.jsonFormat
import com.pajato.persister.readAndPruneData
import java.net.URI

object TestInfoRepo : InfoRepo {
    override val cache: MutableMap<InfoKey, InfoWrapper> = mutableMapOf()
    internal var infoUri: URI? = null
    internal var registerHappened = false

    override suspend fun injectDependency(uri: URI) {
        fun handler(exc: Throwable) { throw exc }
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, InfoWrapper.serializer(), ::getKeyFromItem)
        infoUri = uri
    }

    override suspend fun register(json: String) {
        val uri = infoUri ?: throw InfoRepoError(get(INFO_URI_ERROR_KEY))
        val item = jsonFormat.decodeFromString(InfoWrapper.serializer(), json)
        register(cache, uri, item, json)
    }

    override suspend fun register(item: InfoWrapper) {
        val uri = infoUri ?: throw InfoRepoError(get(INFO_URI_ERROR_KEY))
        val json = jsonFormat.encodeToString(InfoWrapper.serializer(), item)
        register(cache, uri, item, json)
        registerHappened = true
    }

    private fun getKeyFromItem(item: InfoWrapper): InfoKey = when (item) {
        is TimestampedMovie -> InfoKey(InfoType.Movie.name, item.movie.id)
        is TimestampedPerson -> InfoKey(InfoType.Person.name, item.person.id)
        is TimestampedTv -> InfoKey(InfoType.Tv.name, item.tv.id)
    }
}
